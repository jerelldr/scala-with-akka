package com.jerelldr.practice

import scala.concurrent.Future

object AdvancedRecap extends App {

  // partial functions
  val partialFunction: PartialFunction[Int, Int] = {
    case 1 => 42
    case 2 => 65
    case 5 => 999
  }

  val pf = (x: Int) =>
    x match {
      case 1 => 42
      case 2 => 65
      case 5 => 999
  }

  val function: (Int => Int) = partialFunction

  val modifiedList = List(1, 2, 3).map {
    case 1 => 42
    case _ => 0
  }

  // lifting
  val lifted = partialFunction.lift // total function Int => Option[Int]
  lifted(2) //Some(65)

  // orElse

  val pfChain = partialFunction.orElse[Int, Int] {
    case 60 => 9000
  }
  pfChain(5) // 999
  pfChain(60) // 9000
  pfChain(457) // throw a matchError

  // type aliases
  type ReceiveFunction = PartialFunction[Any, Unit]

  def receive: ReceiveFunction = {
    case 1 => println("hello")
    case _ => println("confused...")
  }
  //implicits
  implicit val timeout = 3000
  def setTimeout(f: () => Unit)(implicit timeout: Int) = f()

  setTimeout(() => println("timeout")) // extra parameter list omitted

  //implicit conversions
  // implicit defs
  case class Person(name: String) {
    def greet = "sHi, my name is $name"

  }
  implicit def fronStringToPerson(string: String): Person = Person(string)
  "Peter".greet // fromStringToPerson("Peter .greet - automatically by the compile

  // implicit classes
  implicit class Dog(name: String) {
    def bark = println("bark")

  }
  "Lassie".bark
  // new Dog("Lasses").bark - automatically done by the compiler

  //organize
  implicit val inverseOrdering: Ordering[Int] = Ordering.fromLessThan(_ > _)
  List(1, 2, 3).sorted // List(2,3,1

  // imported scope
  import scala.concurrent.ExecutionContext.Implicits.global
  val future = Future {
    println("hello, future")
  }

  // companion objects of the types included in the call
  object Person {
    implicit val personOrdering: Ordering[Person] =
      Ordering.fromLessThan((a, b) => a.name.compareTo(b.name) < 0)
  }
  List(Person("Bob"), Person("Alice")).sorted // List(Person(Alice), Person(Bob)

}
